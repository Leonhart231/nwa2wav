# The NWA file format

## Introduction
The NWA file format is an optionally compressed audio file format. It is divided up into two or three sections.

The first section is a header containing information about the file such as the number of channels, the bitrate, etc. If the file is compressed, a second section will contain a list of offsets to individual compressed blocks. The final section is either uncompressed audio data the same as a WAV file or many compressed blocks of data.

The rest of this will cover each of these sections in detail. In them, I will consider the NWA file as if it is an array of bytes. So if I talk about index 0, that is the first byte of the file, index 1 is the second byte, etc. Note that all integers are little endian.

## Glossary
| Term | Definition |
| --- | --- |
| Channel | One channel is mono, two is stereo |
| Sample | A single audio reading on a single channel |
| Frame | A single sample from all channels |
| Block | A compressed chunk of data in the file |

## Header
The header takes up the first 44 bytes of an NWA file. It is made up of several integers with meanings as follows

| Starting index | Ending index | Meaning |
| --- | --- | --- |
| 0 | 1 | The number of channels |
| 2 | 3 | The number of bits in an audio sample (after decompression) |
| 4 | 7 | The number of frames per second |
| 8 | 11 | The compression level |
| 12 | 15 | Flag (0 or 1) indicating if run-length encoding is used |
| 16 | 19 | The number of blocks in a compressed file |
| 20 | 23 | The size of the uncompressed data in bytes |
| 24 | 27 | The size of the compressed data in bytes |
| 28 | 31 | Number of samples in the file |
| 32 | 35 | Number of samples in blocks except for the last one |
| 36 | 39 | Number of samples in the last block |
| 40 | 43 | Unknown |

## Offsets
This section may or may not appear depending on the compression level. If the compression level is 0xFFFFFFFF (-1 if you treat it as a signed integer), then no compression is used and the remaining bytes in the file are all of the uncompressed frames.

Otherwise, starting at index 44 in the file, there are a series of four byte integers equal to the number of blocks in the file. Each integer is the index of the next compressed block in the file. There is no padding between the offsets, meaning the first runs from index 44 to 47, the second from 48 to 51, etc.

## Data
If the compression level was -1, then the data is uncompressed. Otherwise, it is compressed.

### Uncompressed
The uncompressed data will have a number of channels and bits per sample as identified in the header. If there are multiple bytes per sample, they are little endian integers. If there are multiple channels, the left channel is the first one to appear.

For example, if you have 2 channels, 16 bits per sample, and the data section begins with the hexadecimal bytes:
```
5a 2b 8c d2 ef 13 4e 64
```

Then the audio data would be:

| Channel | Frame 1 | Frame 2 |
| --- | --- | --- |
| Left | 0x2b5a | 0x13ef |
| Right | 0xd28c | 0x644e |

Again, this is exactly the same format as a standard uncompressed WAV file.

### Compressed
Compressed data is a number of bytes running from the offset defined for a block to the offset defined for the next block. I will call this a "block" of data and will break it down in isolation.

The block is broken up into a couple parts. The first part is what I’ll call the seed and explain later. Then comes the bitstream which is considered bit-by-bit instead of in bytes.

The way the compression algorithm works is this. It begins with an initial seed value for each channel. This is not a sample itself, just a starting point for the bitstream. Then, the bitstream defines changes from  the previous sample, starting with the seed. So, you could start with a seed of 200 and values in the bitstream of -15 and +50. That would give you samples of 185 (200 - 15) and 235 (185 + 50).

Note that the resulting value is always truncated to the number of bits in a sample. So if you have 8 bit data and you do 250 + 10, the answer is not 260, but 4 (binary 1 0000 0100 is 9 bits so the highest one is dropped). Subtraction is handled similarly.

#### Seed
The seed is just a single uncompressed frame as described in the [Uncompressed](#uncompressed) section above. The only difference is it is not an actual audio frame to be output, only the starting values for the bitstream.

All values moving forward will be binary, taken starting with the least significant bit of the current byte. So if we have a binary value of 1101 0110 and take three bits off, that would give 110, the least significant bits. If we then took two more bits, that would be a 10. It is possible that you need more bits than are available in the current byte. In that case, they are taken starting with the least significant bits of the next byte.

#### Bitstream
The bitstream has a number of samples in it as defined by the header. It is a fixed value for all blocks except for the last one which has the remaining samples for the file.

Each frame comes in order and each channel has a sample within the frame. Each channel’s data is considered independently of any other channel’s, meaning the changes from sample-to-sample are considered for each channel individually.

With that done, all that is left is to describe a single sample. A sample is usually divided into multiple parts. First is a 3 bit exponent, then a variable length mantissa, then a 1 bit sign. There are some exceptions to these rules but they will be discussed.

##### Exponent
The exponent is always three bits and roughly defines how much to magnify the mantissa.

If the exponent is 0, this is a special case and can mean one of two things. If run-length encoding is not used based on the NWA header, then the previous sample is repeated once. Otherwise, the next bit(s) define the number of repeats. First, read one bit. If it is 0, repeat once, otherwise, read two more bits. If those bits are not binary 11, they signify the number of repeats. Otherwise, read another eight bits to get the number of repeats.

If the exponent is 7, read one bit. If it is 1, this signifies that the previous sample is repeated.

If any of the above cases apply, the mantissa and sign bits are not read. If the above cases don’t apply, this is a normal case. Use the table below to determine the number of bits and the shift, which will be explained below.

| Exponent | Compression level | Bits | Shift |
| --- | --- | --- | --- |
| 1 to 6 | < 3 | 4 - compression level | 2 + exponent + compression level |
| 1 to 6 | ≥ 3 | 2 + compression level | 1 + exponent |
| 7 | < 3 | 7 - compression level | 9 + compression level |
| 7 | ≥ 3 | 7 | 9 |

##### Mantissa
You read the mantissa by reading a number of bits as defined in the table up above and then left-shifting this value by the number of bits defined in the table.

##### Sign
Read one more bit from the bitstream. If it is 0, then the new sample value is the previous sample value plus the mantissa. Otherwise, you subtract the mantissa instead. Note that the value is always truncated to the sample size, meaning overflow and underflow are ignored.
