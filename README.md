# nwa2wav
nwa2wav version 1.0.1

## About
This is a Python script for converting NWA files to standard WAV files. The NWA file type is an audio file format used in RealLive and SiglusEngine video games such as Clannad to store music and sound effects.

## Installation
nwa2wav is a Python script, so no installation is necessary. All you need is a copy of Python installed. Windows and Mac users can download it from the [Python website](https://www.python.org/) (make sure to add it to your PATH). Linux and BSD users may already have it installed, or you can install it through your distribution’s package system.

> Non-Windows users: You have the ability to install nwa2wav as an executable program. Simply move it to a folder in your PATH and make sure it is marked as executable. You can do this by running `chmod +x nwa2wav`

## Examples
The examples below assume nwa2wav was installed as an executable. If this was not done, either because you are on Windows or chose not to, you will need to make some changes to them:

> Windows users: Replace `nwa2wav` with `py C:\path\to\nwa2wav`.

> Non-Windows users: Replace `nwa2wav` with `python3 /path/to/nwa2wav`.

Note that the program takes a while to run; up to a few minutes for a single file depending on it’s length and contents.

The examples below convert input.nwa to output.wav. You can use any combination of arguments and standard input/output redirection.

### Example 1
You can use arguments to specify the input and output files as follows:

```
nwa2wav -i input.nwa output.wav
```

### Example 2
> Windows users: This will not work on PowerShell since it does not support standard input redirection; use the Command Prompt instead.

You can use standard input and standard output to read and write the files as follows:

```
nwa2wav < input.nwa > output.wav
```

## Confirmed working
The following games’ NWA files have been confirmed to properly convert with this script. If you have the chance to test others, please let me know so I can add them to the list. If you find that a game’s NWA files do not properly convert, please add an issue or email me at renters-wounds-0f at icloud.com with the information.
- Clannad (Steam version)
- Clannad Side Stories (Steam version)
- Planetarian (Steam version)
- Tomoyo After (Steam version)

## Why?
While working on this, I ran into two programs that convert NWA files and more.

First, there is [rlvm](https://github.com/eglaysher/rlvm/) that acts as a RealLive engine clone for non-Windows platforms. I used the comments and code in the [NWA conversion file](https://github.com/eglaysher/rlvm/blob/master/vendor/xclannad/nwatowav.cc) to help write this program. Unfortunately, it has not had a release since 2014 and I was unable to get it to install on my own computer due to some old dependencies.

Second is [SiglusExtract](https://github.com/xmoeproject/SiglusExtract/) which is intended to extract the contents of SiglusEngine games. However, it requires Visual Studio to compile and only supports Windows.

In the end, I wanted to write a simple program that could run on any operating system to make it easier for people to extract game audio. Not everyone has a C++ compiler and knows how to use it, so I didn’t want that to be a limiting factor. I also wanted to take the opportunity to document the NWA file format in English in case anyone else wants to do any work on the files.

One downside of using Python instead of C++ is that it is very slow by comparison. For example, the music for Clannad Side Stories takes about an hour to convert on my computer. That said, I think the benefits outweigh the costs.
