# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/),
and this project adheres to [Semantic Versioning](https://semver.org/).

## [Unreleased] - YYYY-MM-DD
### Changed
- Minor code changes for clarity

## [1.0.0] - 2020-06-27
### Added
- Completed NWA format description
### Changed
- Simplified, commented, and improved nwa2wav code
### Fixed
- Fixed a bug where exponent bits would be read during an RLE’d period

## [0.2.0] - 2020-06-26
### Added
- Command line arguments [-h] [--version] [-i INPUT] [OUTPUT]
- Another example in README.md
### Changed
- README.md instructions for Windows

## [0.1.0] - 2020-06-20
### Added
- Initial nwa2wav version
- Partially complete NWA Format.md document
- README.md
- LICENSE
- This file, CHANGELOG.md
- Partial NWA format description
